//
//  CameraViewController.swift
//  NGDR-iOS
//
//  Created by Nikita Elizarov on 04/09/2018.
//  Copyright © 2018 ITRI. All rights reserved.
//

import UIKit
import Vision
import AVFoundation
import CoreMedia
import VideoToolbox

class CameraViewController: UIViewController {
    @IBOutlet weak var videoPreview: UIView!

    private let yolo = YOLO()
    
    private var videoCapture: VideoCapture!
    private var request: VNCoreMLRequest!

    private var startTimes: [CFTimeInterval] = []

    private var timerSeconds = 4
    private var isTimerRunning = false
    private var timer = Timer()
    private var predictionCounter: Int = 0
    private var predictionLimit:Int?

    private var boundingBoxes = [BoundingBox]()
    private var colors: [UIColor] = []

    private let ciContext = CIContext()
    private var resizedPixelBuffer: CVPixelBuffer?

    private var framesDone = 0
    private var frameCapturingStartTime = CACurrentMediaTime()
    private let semaphore = DispatchSemaphore(value: 2)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isToolbarHidden = true

        setUpPredictionLimit()
        setUpBoundingBoxes()
        setUpCoreImage()
        setUpVision()
        setUpCamera()

        frameCapturingStartTime = CACurrentMediaTime()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print(#function)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.videoCapture.stop()
    }

    // MARK: - Initialization
    
    func setUpPredictionLimit(){
        let deviceType = UIDevice().type
        switch deviceType {
        case .iPhone6:
            predictionLimit = 3
        default:
            predictionLimit = 10
        }
        
    }

    func setUpBoundingBoxes() {
        for _ in 0..<YOLO.maxBoundingBoxes {
            boundingBoxes.append(BoundingBox())
        }

        // Make colors for the bounding boxes. There is one color for each class,
        // 80 classes in total.
        for r: CGFloat in [0.2, 0.4, 0.6, 0.8, 1.0] {
            for g: CGFloat in [0.3, 0.7, 0.6, 0.8] {
                for b: CGFloat in [0.4, 0.8, 0.6, 1.0] {
                    let color = UIColor(red: r, green: g, blue: b, alpha: 1)
                    colors.append(color)
                }
            }
        }
    }

    func setUpCoreImage() {
        let status = CVPixelBufferCreate(nil, YOLO.inputWidth, YOLO.inputHeight,
                kCVPixelFormatType_32BGRA, nil,
                &resizedPixelBuffer)
        if status != kCVReturnSuccess {
            print("Error: could not create resized pixel buffer", status)
        }
    }

    func setUpVision() {
        guard let visionModel = try? VNCoreMLModel(for: yolo.model.model) else {
            print("Error: could not create Vision model")
            return
        }

        request = VNCoreMLRequest(model: visionModel, completionHandler: visionRequestDidComplete)

        // NOTE: If you choose another crop/scale option, then you must also
        // change how the BoundingBox objects get scaled when they are drawn.
        // Currently they assume the full input image is used.
        request.imageCropAndScaleOption = .centerCrop
    }

    func setUpCamera() {
        videoCapture = VideoCapture()
        videoCapture.delegate = self
        videoCapture.fps = 60
        videoCapture.setUp(sessionPreset: AVCaptureSession.Preset.hd1920x1080) { success in
            if success {
                // Add the video preview into the UI.
                if let previewLayer = self.videoCapture.previewLayer {
                    self.videoPreview.layer.addSublayer(previewLayer)
                    self.resizePreviewLayer()
                }

                // Add the bounding box layers to the UI, on top of the video preview.
                for box in self.boundingBoxes {
                    box.addToLayer(self.videoPreview.layer)
                }

                // Once everything is set up, we can start capturing live video.
                self.videoCapture.start()
            }
        }
    }

    // MARK: - UI stuff

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func resizePreviewLayer() {
        videoCapture.previewLayer?.connection?.videoOrientation = .portrait
        videoCapture.previewLayer?.frame = videoPreview.bounds
    }

//   MARK: - Doing inference

    func predict(image: UIImage) {
        if let pixelBuffer = image.pixelBuffer(width: YOLO.inputWidth, height: YOLO.inputHeight) {
            predict(pixelBuffer: pixelBuffer)
        }
    }

    func predict(pixelBuffer: CVPixelBuffer) {
        // Resize the input with Core Image to 416x416.
        guard let resizedPixelBuffer = resizedPixelBuffer else {
            return
        }
        let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
        let sx = CGFloat(YOLO.inputWidth) / CGFloat(CVPixelBufferGetWidth(pixelBuffer))
        let sy = CGFloat(YOLO.inputHeight) / CGFloat(CVPixelBufferGetHeight(pixelBuffer))
        let scaleTransform = CGAffineTransform(scaleX: sx, y: sy)
        let scaledImage = ciImage.transformed(by: scaleTransform)
        ciContext.render(scaledImage, to: resizedPixelBuffer)

//         This is an alternative way to resize the image (using vImage):
//        let resizedPixelBuffer = resizePixelBuffer(pixelBuffer, width: YOLO.inputWidth, height: YOLO.inputHeight)

        // Resize the input to 416x416 and give it to our model.
        if let boundingBoxes = try? yolo.predict(image: resizedPixelBuffer) {
            showOnMainThread(boundingBoxes)
        }
    }
    
    func predictUsingVision(pixelBuffer: CVPixelBuffer) {
        // Vision will automatically resize the input image.
        let handler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer)
        try? handler.perform([request])
    }

    func visionRequestDidComplete(request: VNRequest, error: Error?) {
        if let observations = request.results as? [VNCoreMLFeatureValueObservation],
           let features = observations.first?.featureValue.multiArrayValue {

            let boundingBoxes = yolo.computeBoundingBoxes(features: [features, features])
            
            //For YOLOv3
            //let boundingBoxes = yolo.computeBoundingBoxes(features: [features, features, features])
            
            showOnMainThread(boundingBoxes)
        }
    }

    func showOnMainThread(_ boundingBoxes: [YOLO.Prediction]) {

        DispatchQueue.main.async {

            self.show(predictions: boundingBoxes)

            self.capturePhotoOnPrediction(predictions: boundingBoxes)

            self.semaphore.signal()
        }
    }

    func show(predictions: [YOLO.Prediction]) {
        for i in 0..<boundingBoxes.count {
            if i < predictions.count {
                let prediction = predictions[i]

                // The predicted bounding box is in the coordinate space of the input
                // image, which is a square image of 416x416 pixels. We want to show it
                // on the video preview, which is as wide as the screen and has a 4:3
                // aspect ratio. The video preview also may be letterboxed at the top
                // and bottom.
                let width = view.bounds.width
                let height = width * 4 / 3
                let scaleX = width / CGFloat(YOLO.inputWidth)
                let scaleY = height / CGFloat(YOLO.inputHeight)
                let top = (view.bounds.height - height) / 2

                // Translate and scale the rectangle to our own coordinate system.
                var rect = prediction.rect
                rect.origin.x *= scaleX
                rect.origin.y *= scaleY
                rect.origin.y += top
                rect.size.width *= scaleX
                rect.size.height *= scaleY

                // Show the bounding box.
                let label = String(format: "%@ %.1f", labels[prediction.classIndex], prediction.score * 100)
                let color = colors[prediction.classIndex]
                boundingBoxes[i].show(frame: rect, label: label, color: color)
            } else {
                boundingBoxes[i].hide()
            }
        }
    }

    //MARK: - PhotoProccessing

    // Basing on the current prediction on the screen set a timer for picture if the prediction is about a certain score
    private func capturePhotoOnPrediction(predictions: [YOLO.Prediction]) {
        for i in 0..<boundingBoxes.count {
            if i < predictions.count {
                let prediction = predictions[i]
                let predictionObjectScore = prediction.score * 100
                let predictionObjectName = labels[prediction.classIndex]
                let predictedObjectInfo = String(format: "%@ %.1f", predictionObjectName, predictionObjectScore)

                if (predictionObjectName == "OK" && predictionObjectScore > 85.0) {
                    predictionCounter += 1
                    if (predictionCounter == predictionLimit!) {
                        if (isTimerRunning == false) {
                            runTimer()
                        }
                    }
                } else if ((predictionObjectName == "OK" || predictionObjectName == "Hand") && predictionObjectScore <= 85.0) {
                    predictionCounter = 0
                }
                
                print(predictedObjectInfo)
            }
        }
    }

    private func capturePicture() {
        predictionCounter = 0
        videoCapture.captureImage { (image, error) in
            guard let image = image else {
                print(error ?? "Image capture error")
                return
            }
            try? DataHandler.savePhotoRecord(image: image)
        }

    }

    private func runTimer() {
        isTimerRunning = true
        timerSeconds = 5
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(CameraViewController.updateTimer)), userInfo: nil, repeats: true)
    }

    @objc private func updateTimer() {
        if (timerSeconds == 0) {
            timer.invalidate()
            isTimerRunning = false
            capturePicture()
        } else {
            timerSeconds -= 1
            if (timerSeconds > 0) {
                AudioServicesPlayAlertSound(SystemSoundID(1322))
            }
        }
    }

    @IBAction func takeAPhoto(_ sender: Any) {
        capturePicture()
    }

}

extension CameraViewController: VideoCaptureDelegate {
    func videoCapture(_ capture: VideoCapture, didCaptureVideoFrame pixelBuffer: CVPixelBuffer?, timestamp: CMTime) {
        
        semaphore.wait()

        if let pixelBuffer = pixelBuffer {
            // For better throughput, perform the prediction on a background queue
            // instead of on the VideoCapture queue. We use the semaphore to block
            // the capture queue and drop frames when Core ML can't keep up.
            DispatchQueue.global().async {
                self.predict(pixelBuffer: pixelBuffer)
            }
        }
    }
}
