//
//  GridViewController.swift
//  YOLOv3-CoreML
//
//  Created by Nikita Elizarov on 04/09/2018.
//  Copyright © 2018 ITRI. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let store = PhotoStore.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        store.fillPhotoContent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        store.emptyPhotoContent()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(store.photoCollection.count)
        return store.photoCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! GalleryCollectionViewCell
        
        let photoURL = store.photoCollection[indexPath.row]
        
        cell.dispalyContent(image: UIImage(contentsOfFile: photoURL)!)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCell = collectionView.cellForItem(at: indexPath) as! GalleryCollectionViewCell
        
    }

}
