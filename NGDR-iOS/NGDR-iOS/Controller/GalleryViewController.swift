//
//  GalleryViewController.swift
//  NGDR-iOS
//
//  Created by Nikita Elizarov on 04/09/2018.
//  Copyright © 2018 ITRI. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var photoGallery: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func fillPhotoGallery() -> [String]{
        var photoCollection: [String] = []
        let fetchedData = DataHandler.retrievePhotoRecords()
        photoCollection.append(contentsOf: fetchedData)
        return photoCollection
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = true
        photoGallery = fillPhotoGallery()
        collectionView.reloadData()
        super.viewWillAppear(true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 3) - 20), height: CGFloat(100))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoGallery.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! GalleryCollectionViewCell
        
        let photoURL = photoGallery[indexPath.row]
        
        cell.dispalyContent(image: UIImage(contentsOfFile: photoURL)!)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        
        self.performSegue(withIdentifier: "showPreviewPhoto", sender: cell)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPreviewPhoto"{
            if let cell = sender as? UICollectionViewCell,
                let indexPath = collectionView.indexPath(for: cell){
                let selectedCell = photoGallery[indexPath.row]
                print(indexPath.row)
                let previewVC = segue.destination as! PreviewViewController
                previewVC.passedPhotoPath = selectedCell
            }
        }
    }
    
}
