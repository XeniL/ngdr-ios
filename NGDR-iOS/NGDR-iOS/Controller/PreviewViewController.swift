//
//  PreviewViewController.swift
//  NGDR-iOS
//
//  Created by Nikita Elizarov on 04/09/2018.
//  Copyright © 2018 ITRI. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {

    @IBOutlet weak var photoPreview: UIImageView!

    var passedPhotoPath: String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isToolbarHidden = false

        if passedPhotoPath != nil {
            photoPreview.image = UIImage(contentsOfFile: passedPhotoPath!)
        }

    }

    private func showAlertOnDelete() {
        let alert = UIAlertController(title: "Delete the photo", message: "Are you sure you want to delete this photo?", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in self.deletePhoto() }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }

    private func deletePhoto() {
        DataHandler.deletePhotoRecord(filePath: passedPhotoPath!)
        navigationController?.popViewController(animated: true)
    }

    @IBAction func shareImage(_ sender: Any) {
        let activityController = UIActivityViewController(activityItems: [UIImage(contentsOfFile: passedPhotoPath!)!], applicationActivities: nil)

        self.present(activityController, animated: true, completion: nil)
    }

    @IBAction func deleteImage(_ sender: Any) {
        showAlertOnDelete()
    }
}

extension UINavigationController {

    private func doAfterAnimatingTransition(animated: Bool, completion: @escaping (() -> Void)) {
        if let coordinator = transitionCoordinator, animated {
            coordinator.animate(alongsideTransition: nil, completion: { _ in
                completion()
            })
        } else {
            DispatchQueue.main.async {
                completion()
            }
        }
    }

    func popViewController(animated: Bool, completion: @escaping (() -> Void)) {
        popViewController(animated: animated)
        doAfterAnimatingTransition(animated: animated, completion: completion)
    }
}
