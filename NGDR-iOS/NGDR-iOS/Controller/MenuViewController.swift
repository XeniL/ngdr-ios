//
//  MenuViewController.swift
//  NGDR-iOS
//
//  Created by Nikita Elizarov on 04/09/2018.
//  Copyright © 2018 ITRI. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = true
        super.viewWillAppear(true)
    }


}
