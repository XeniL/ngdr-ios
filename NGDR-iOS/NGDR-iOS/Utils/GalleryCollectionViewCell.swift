//
//  GalleryCollectionViewCell.swift
//  YOLOv3-CoreML
//
//  Created by Nikita Elizarov on 04/09/2018.
//  Copyright © 2018 MachineThink. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var photoImage: UIImageView!

    func dispalyContent(image: UIImage) {
        photoImage.image = image
    }

}
