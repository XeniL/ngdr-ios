//
//  DataHandler.swift
//  YOLOv3-CoreML
//
//  Created by Nikita Elizarov on 05/09/2018.
//  Copyright © 2018 MachineThink. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DataHandler {
    static let appDelegate = UIApplication.shared.delegate! as! AppDelegate

    fileprivate init() {
    }

    static func savePhotoRecord(image: UIImage) throws {
        let fileManager = FileManager.default
        let documentURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!

        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Photo", in: managedContext)!
        let photo = NSManagedObject(entity: entity, insertInto: managedContext)

        do {
            let filePath = documentURL.appendingPathComponent("\(image.hash).png")

            let rotatedImage = image.rotate(radians: .pi * 2)

            let pngImageData = UIImage.pngData(rotatedImage!)
            try pngImageData()?.write(to: filePath)

            photo.setValue(filePath.path, forKey: "filePath")

            try managedContext.save()

        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    static func retrievePhotoRecords() -> [String] {
        var photoPathArray: [String] = []
        let managedContext = appDelegate.persistentContainer.viewContext
        let photoFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Photo")
        do {
            let photos = try managedContext.fetch(photoFetch)
            for data in photos as! [NSManagedObject] {
                let photoPathEntity = data.value(forKey: "filePath") as! String
                if FileManager.default.fileExists(atPath: photoPathEntity) {
                    photoPathArray.append(photoPathEntity)
                }
            }

            return photoPathArray

        } catch {
            print("Request for image fetch failed")
        }

        return []
    }

    static func deletePhotoRecord(filePath: String) {
        let fileManager = FileManager.default
        let managedContext = appDelegate.persistentContainer.viewContext
        let photoFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Photo")

        do {
            let photos = try managedContext.fetch(photoFetch)
            for data in photos as! [NSManagedObject] {
                let photoPathEntity = data.value(forKey: "filePath") as! String
                if photoPathEntity == filePath {
                    managedContext.delete(data)
                    try? fileManager.removeItem(atPath: filePath)
                }
            }

            try managedContext.save()

        } catch let error as NSError {
            print("Could not delete the record. \(error), \(error.userInfo)")
        }
    }

}


extension UIImage {
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, true, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width / 2, y: newSize.height / 2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))

        self.draw(in: CGRect(x: -self.size.width / 2, y: -self.size.height / 2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
}
